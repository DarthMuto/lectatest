using System;
using System.Collections.Generic;

namespace LectaTest.CalculationTasks
{
    public abstract class BaseCalculationTask
    {
        protected readonly List<Matrix> Operands = new List<Matrix>();

        public Action<int, int> ProgressCallback;

        public void AddOperand(Matrix operand)
        {
            Operands.Add(operand);
        }

        public void AddOperand(IEnumerable<string> matrixLines)
        {
            var matrix = new Matrix(matrixLines);
            AddOperand(matrix);
        }

        public abstract IEnumerable<Matrix> Calculate();

        protected void _checkOperandsQuantity(int minQuantity)
        {
            if (Operands.Count < minQuantity)
                throw new Exception("Too few operands added");
        }

        protected void ProgressTick(int processed, int total)
        {
            ProgressCallback?.Invoke(processed, total);
        }
    }
}