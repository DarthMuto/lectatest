using System.Collections.Generic;

namespace LectaTest.CalculationTasks
{
    public class MultiplyCalculationTask : BaseCalculationTask
    {
        public override IEnumerable<Matrix> Calculate()
        {
            _checkOperandsQuantity(2);
            
            var result = Operands[0];

            for (var i = 1; i < Operands.Count; i++)
            {
                result = result.Multiply(Operands[i]);
                ProgressTick(i, Operands.Count - 1);
            }

            return new List<Matrix> {result};
        }
    }
}