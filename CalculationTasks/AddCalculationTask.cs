using System.Collections;
using System.Collections.Generic;

namespace LectaTest.CalculationTasks
{
    public class AddCalculationTask : BaseCalculationTask
    {
        public override IEnumerable<Matrix> Calculate()
        {
            _checkOperandsQuantity(2);
            
            var result = Operands[0];

            for (var i = 1; i < Operands.Count; i++)
            {
                result = _add(result, Operands[i]);
                ProgressTick(i, Operands.Count - 1);
            }

            return new List<Matrix> {result};
        }
        
        protected virtual Matrix _add(Matrix a, Matrix b)
        {
            return a.Add(b);
        }
    }
}