namespace LectaTest.CalculationTasks
{
    public enum Operation
    {
        Multiply,
        Add,
        Subtract,
        Transpose
    }
}