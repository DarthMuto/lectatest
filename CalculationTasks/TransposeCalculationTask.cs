using System.Collections;
using System.Collections.Generic;

namespace LectaTest.CalculationTasks
{
    public class TransposeCalculationTask : BaseCalculationTask
    {
        public override IEnumerable<Matrix> Calculate()
        {
            _checkOperandsQuantity(1);
            
            var result = new List<Matrix>();

            for (var i = 0; i < Operands.Count; i++)
            {
                result.Add(Operands[i].Transpose());
                ProgressTick(i + 1, Operands.Count);
            }

            return result;
        }
    }
}