namespace LectaTest.CalculationTasks.Factories
{
    public class TransposeCalculationTaskFactory : BaseCalculationTaskFactory
    {
        public override BaseCalculationTask CreateTask()
        {
            return new TransposeCalculationTask();
        }
    }
}