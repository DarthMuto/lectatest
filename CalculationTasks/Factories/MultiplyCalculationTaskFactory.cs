namespace LectaTest.CalculationTasks.Factories
{
    public class MultiplyCalculationTaskFactory : BaseCalculationTaskFactory
    {
        public override BaseCalculationTask CreateTask()
        {
            return new MultiplyCalculationTask();
        }
    }
}