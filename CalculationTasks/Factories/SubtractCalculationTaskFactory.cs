namespace LectaTest.CalculationTasks.Factories
{
    public class SubtractCalculationTaskFactory : BaseCalculationTaskFactory
    {
        public override BaseCalculationTask CreateTask()
        {
            return new SubtractCalculationTask();
        }
    }
}