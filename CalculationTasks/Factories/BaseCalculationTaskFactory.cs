namespace LectaTest.CalculationTasks.Factories
{
    public abstract class BaseCalculationTaskFactory
    {
        public abstract BaseCalculationTask CreateTask();
    }
}