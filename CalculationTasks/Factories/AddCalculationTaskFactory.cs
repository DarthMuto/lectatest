namespace LectaTest.CalculationTasks.Factories
{
    public class AddCalculationTaskFactory : BaseCalculationTaskFactory
    {
        public override BaseCalculationTask CreateTask()
        {
            return new AddCalculationTask();
        }
    }
}