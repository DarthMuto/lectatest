using System;
using System.Collections.Generic;
using LectaTest.CalculationTasks.Factories;

namespace LectaTest.CalculationTasks
{
    public class TaskLoader
    {
        private readonly Dictionary<Operation, BaseCalculationTaskFactory> _calcTaskFactories;

        public TaskLoader()
        {
            _calcTaskFactories = new Dictionary<Operation, BaseCalculationTaskFactory>
            {
                {Operation.Add, new AddCalculationTaskFactory()},
                {Operation.Subtract, new SubtractCalculationTaskFactory()},
                {Operation.Multiply, new MultiplyCalculationTaskFactory()},
                {Operation.Transpose, new TransposeCalculationTaskFactory()}
            };
        }

        private BaseCalculationTask _createCalculationTask(Operation operation)
        {
            return _calcTaskFactories[operation].CreateTask();
        }

        public BaseCalculationTask LoadTask(string[] lines)
        {
            if (!Enum.TryParse(lines[0], true, out Operation operation))
                throw new Exception("Invalid operation");
            
            if (lines[1] != "")
                throw new Exception("Format mismatch, next line after operation name is expected to be blank");
            
            var task = _createCalculationTask(operation);
            
            var matrixLines = new List<string>();
            for (var i = 2; i < lines.Length; i++)
            {
                if (lines[i] == "")
                {
                    task.AddOperand(matrixLines);
                    matrixLines.Clear();
                    continue;
                }
                matrixLines.Add(lines[i]);
            }
            if (matrixLines.Count > 0)
                task.AddOperand(matrixLines);
            
            return task;
        }
    }
}