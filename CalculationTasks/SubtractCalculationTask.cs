namespace LectaTest.CalculationTasks
{
    public class SubtractCalculationTask : AddCalculationTask
    {
        protected override Matrix _add(Matrix a, Matrix b)
        {
            return a.Subtract(b);
        }
    }
}