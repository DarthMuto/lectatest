using System;
using System.Collections.Generic;
using System.IO;
using LectaTest.CalculationTasks;

namespace LectaTest
{
    public class CalculationTaskList
    {
        private readonly string[] _files;
        private readonly TaskLoader _taskLoader;

        public Action<string> TaskStarted;
        public Action<string> TaskFinished;
        public Action<int, int> TaskProgress;

        public CalculationTaskList(string path)
        {
            _files = Directory.GetFiles(path);
            _taskLoader = new TaskLoader();
        }

        public void ProcessTasks()
        {
            foreach (var file in _files)
            {
                try
                {
                    ProcessFile(file);
                }
                catch (Exception e)
                {
                    Console.Error.WriteLine("Error while processing {0}: {1}", file, e.Message);
                }
            }
        }

        private static string OutFileName(string inFile)
        {
            return Path.GetDirectoryName(inFile)
                   + Path.DirectorySeparatorChar
                   + Path.GetFileNameWithoutExtension(inFile)
                   + "_result.txt";
        }

        private void ProcessFile(string fileName)
        {
            TaskStarted?.Invoke(fileName);
            
            var text = File.ReadAllLines(fileName);
            var task = _taskLoader.LoadTask(text);
            task.ProgressCallback = TaskProgress;
            var results = task.Calculate();

            var output = new List<string>();
            foreach (var matrix in results)
            {
                output.AddRange(matrix.ToStringList());
                output.Add("");
            }

            File.WriteAllLines(OutFileName(fileName), output);
            
            TaskFinished?.Invoke(fileName);
        }
        
    }
}