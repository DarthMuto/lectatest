﻿using System;

namespace LectaTest
{
    class Program
    {
        static void Main(string[] args)
        {
            var dirName = args.Length > 0 ? args[0] : "TestData";

            var tasksList = new CalculationTaskList(dirName)
            {
                TaskStarted = file => Console.WriteLine("Processing {0}", file),
                TaskProgress = (processed, total) =>
                    Console.WriteLine("{0} of {1} operations complete", processed, total),
                TaskFinished = file => Console.WriteLine("Finished with {0}", file)
            };

            tasksList.ProcessTasks();
        }
    }
}
