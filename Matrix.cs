using System;
using System.Collections.Generic;
using System.Linq;

namespace LectaTest
{
    public class Matrix
    {
        private readonly int[,] _values;
        private readonly int _numRows;
        private readonly int _numColumns;
        private (int, int) Shape => (_numRows, _numColumns);

        public Matrix(int numRows, int numColumns)
        {
            _numRows = numRows;
            _numColumns = numColumns;
            _values = new int[numRows, numColumns];
        }

        public Matrix(IEnumerable<string> rows)
        {
            var parsed = rows.Select(r => r.Trim().Split(" ")
                .Select(int.Parse).ToArray()).ToArray();
            _validateParsedShape(parsed);
            
            _numRows = parsed.Length;
            _numColumns = parsed[0].Length;
            _values = new int[_numRows, _numColumns];
            
            for (var i = 0; i < _numRows; i++)
            for (var j = 0; j < _numColumns; j++)
                _values[i, j] = parsed[i][j];
        }

        private static void _validateParsedShape(int[][] parsed)
        {
            if (parsed.Length == 0)
                throw new Exception("Too few rows");

            var lengths = parsed.Select(r => r.Length).ToArray();
            if (lengths.Min(l => l) != lengths.Max(l => l))
                throw new Exception("Elements count varies for different rows");
            
            if (lengths[0] == 0)
                throw new Exception("Too few columns");
        }
        
        public List<string> ToStringList()
        {
            var lines = new List<string>();
            
            for (var i = 0; i < _numRows; i++)
            {
                var line = new List<string>();
                for (var j = 0; j < _numColumns; j++)
                    line.Add(_values[i, j].ToString());
                lines.Add(string.Join(" ", line.ToArray()));
            }

            return lines;
        }

        private Matrix _add(Matrix other, int sign)
        {
            if (Shape != other.Shape)
                throw new Exception("Shape mismatch");
            var result = new Matrix(_numRows, _numColumns);
            for (var i = 0; i < _numRows; i++)
            for (var j = 0; j < _numColumns; j++)
                result._values[i, j] = _values[i, j] + other._values[i, j] * sign;
            return result;
        }

        public Matrix Multiply(Matrix other)
        {
            if (_numColumns != other._numRows)
                throw new Exception("Shape mismatch");
            
            var result = new Matrix(_numRows, other._numColumns);
            
            for (var i = 0; i < result._numRows; i++)
                for (var j = 0; j < result._numColumns; j++)
                for (var k = 0; k < _numColumns; k++)
                    result._values[i, j] += _values[i, k] * other._values[k, j];

            return result;
        }
        
        public Matrix Add(Matrix other)
        {
            return _add(other, 1);
        }

        public Matrix Subtract(Matrix other)
        {
            return _add(other, -1);
        }

        public Matrix Transpose()
        {
            var result = new Matrix(numRows: _numColumns, numColumns: _numRows);
            for (var i = 0; i < _numRows; i++)
            for (var j = 0; j < _numColumns; j++)
                result._values[j, i] = _values[i, j];
            return result;
        }
    }
}